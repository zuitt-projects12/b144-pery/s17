console.log("Array Manipulation");

// Basic array structure
// Access elements in an Array - thru index.

/*
	Two ways to initialize an array.
*/

let array = [1, 2, 3];
console.log(array);

let arr = new Array(1, 2, 3);
console.log(arr);

// index = array.length - 1
console.log(`array: ${array[0]}`);
console.log(`array: ${array[1]}`);
console.log(`array: ${array[2]}`);

// Array Manipulation
let count = ["one", "two", "three", "four"];

console.log(count.length);
console.log(count[4]);

// Adding a new element thru assignment

count[4] = "five";
console.log(count[4]);

// Reassignment
/*count[0] = "element";
console.log(count);*/

// Adding an element thru push() method
/* Adds an element at the end of an array. */
/*
	Syntax:
	array.push(element)
*/

console.log(count.push("element"));
console.log(count);

function pushMethod(element){
	return count.push(element);
}
pushMethod("six");
pushMethod("seven");
pushMethod("eight");
console.log(count);

// Removing an element thru pop() method
/*
	Syntax:
	array.pop()
*/

/*NOTE: pop() will not remove a specific element.*/
count.pop();
console.log(count);
function popMethod(){
	return count.pop();
}
popMethod();
console.log(count);

/*Adding an element at the beginning of an array thru unshift() method*/
/*
	Syntax:
	array.unshift(element);
*/

count.unshift("hugot");
console.log(count);

function unshiftMethod(element){
	return count.unshift(element);
}

unshiftMethod(0);
console.log(count);

/*Removing an element at the beginning of an array thru shift() method*/
/*
	Syntax:
	array.shift();
*/

count.shift();
console.log(count);
function shiftMethod(){
	return count.shift();
}

shiftMethod();
console.log(count);

/*sort method
	Syntax:
	array.sort();
*/

let nums = [15, 32, 61, 130, 230, 13, 34];
/*nums.sort();
console.log(nums);*/

/*sort in ascending order*/
nums.sort(
	function(a, b){
		return a - b
	});

console.log(nums);

/*sort in descending order*/
nums.sort(
	function(a, b){
		return b - a
	});
console.log(nums);

// Reverse method
/*
	Syntax:
	array.reverse();
*/

nums.reverse();
console.log(nums);

/* Splice and slice methods */

/*
	Splice method - splits the array into different variables

	returns an array of omitted elements.

	It directly manipulates the original array.

	Syntax:
	array.splice(index, numOfElements, elementsToBeAdded);

	first parameter - where to start omitting element.
	second parameter - number of elements to be omitted starting from first parameter given.
	third parameter - elements to be added in place of the omitted elements.
*/

// console.log(count);
/*let newSplice = count.splice(1);
console.log(newSplice);
console.log(count);*/

/*let newSplice = count.splice(1, 2);
console.log(newSplice);
console.log(count);*/

/*let newSplice = count.splice(1, 2, "a1", "b2", "c3");
console.log(newSplice);
console.log(count);*/

/*
	Slice method - creates a new copy of array then manipulates it using parameters. It does not affect the original array.

	Syntax:
	array.slice(start, end);

	first parameter - index where to begin omitting elements
	second parameter - number of elements to be removed (index - 1).
*/
console.log(count);
/*let newSlice = count.slice(1);
console.log(newSlice);
console.log(count);*/

let newSlice = count.slice(2, 3);
console.log(newSlice);
console.log(count);

/*Concat method - used to merge two or more arrays*/
/*
	syntax:
	array.concat();
*/
console.log(count);
console.log(nums);
let animals = ["bird", "cat", "dog", "fish"];

let newConcat = count.concat(nums, animals);
console.log(newConcat);

/*Join method - joins elements of array into one single variable.*/
/*
	syntax:
	array.join(separatorCharacter);
*/
let meal = ["rice", "steak", "juice"];
let newJoin = meal.join("");
console.log(newJoin);
newJoin = meal.join("");
console.log(newJoin);
newJoin = meal.join(" ");
console.log(newJoin);
newJoin = meal.join("-");
console.log(newJoin);

/*toString - converts array to string

	syntax:
	array.toString();

*/

console.log(nums);
console.log(typeof nums[3]);

let newString = nums.toString();
console.log(newString);

/*Accessors*/
let countries = ["US", "PH", "CA", "PH", "SG", "HK", "PH", "HK"];

// indexOf() - to find the index of the matching element where it is FIRST found.
/*
	syntax:
	array.indexOf();
*/
let index = countries.indexOf("PH");
console.log(index);

// If the element does not exist, then it will return -1
let example = countries.indexOf("AU");
console.log(example);

// lastIndexOf() - to find the index of the matching element where it is LAST found.
/*
	syntax:
	array.lastIndexOf();
*/
let lastIndex = countries.lastIndexOf("PH");
console.log(lastIndex);

/*if(countries.indexOf("CA") === -1){
	console.log(`Element not existing`);
}
else{
	console.log(`Element exists in the countries array`);
}
*/
/*Iterators*/

/*
	forEach(cb())	array.forEach()
		- returns undefined
	map(cb())		array.map()
		- returns a copy of the original array.
	filter(cb())	array.filter()
		- returns an array that meets a certain condition
	includes(cb())	array.includes()
*/

// forEach()
let days = ["mon", "tue", "wed", "thu", "fri", "sat", "sun"];

days.forEach(
	function(element){
		console.log(element)
	}
);

// map - returns a copy from the original array that can be modified.
let mapDays = days.map(function(day){
	return `${day} is the day of the week`
});

console.log(mapDays);
// console.log(days);

let days2 = [];
console.log(days2);
days.forEach(function(day){
	days2.push(day)
});

console.log(days2);

// filter()

console.log(nums);
let newFilter = nums.filter(function(num){
	return num < 50
});

console.log(newFilter);

// includes()

console.log(animals);
let newIncludes = animals.includes("dog");

console.log(newIncludes);

function isFound(name){
	if (animals.includes(name)){
		return `${name} is found`;
	}
	return `${name} is not found`;
}

console.log(isFound("cat"));
console.log(isFound("ex"));
console.log(isFound("fish"));

// every(cb()) - returns boolean
/*every - returns true only if ALL ELEMENTS has the given condition.*/
let newEvery = nums.every(function(num){
	return (num > 50)
});


console.log(newEvery);

// some - return boolean

let newSome = nums.some(function(num){
	return (num > 50)
});

console.log(newSome);
/*ARROW FUNCTION*/
let newSome2 = nums.some(num => num > 50);

console.log(newSome2);

// reduce(cb(<previousValue>, <currentValue>)) - performs arithmetic operations and returing its value.

let newReduce = nums.reduce(function(a, b){
	// return b - a;
	return a + b
});
console.log(newReduce);


// to get the average of nums array.
	// total all the elements
	// divide by nums.length

let average = newReduce / nums.length;
// ----------------------
// toFixed(#_of_decimals) - returns string

console.log(average.toFixed(2));

// parseInt() - not keep decimal and round off (if necessary)

console.log(parseInt(average.toFixed(2)));

// parseFloat() - keep decimal place
console.log(parseFloat(average.toFixed(2)));
