let students = new Array();

function addStudent(name){
	if (typeof name === "string"){
		students.push(name);
	}
	else if (typeof name === "object"){
		name.forEach(function(n){
			students.push(n)
		});
	}
	return `${name} was added to the student's list.`;
}

function countStudents(){
	return students.length;
}

function printStudents(){
	let sortStudents = students.sort();
	sortStudents.forEach(function(student){
		console.log(student)
	});
}

function findStudent(name){
	let foundStudents = students.filter(function(student){
		return student.toLowerCase().includes(name.toLowerCase())
	});
	/*students.filter(function(student){
		if(name.toLowerCase() === student.toLowerCase()){
			foundStudents.push(student);
		}
		else{
			let studentSplit = student.toLowerCase().split("");
			let nameSplit = name.toLowerCase().split("");
			let matchLetters = new Array();
			for(let i = 0; i < nameSplit.length; i++){
				if(studentSplit[i] === nameSplit[i]){
					matchLetters.push(nameSplit[i]);
				}
			}
			if(matchLetters.length === nameSplit.length){
				foundStudents.push(student);
			}
		}
	});*/
	if (foundStudents.length === 1){
		/*If ONLY one match is found.*/
		return `${foundStudents.toString()} is an enrollee.`;
	}
	else if (foundStudents.length > 1){
		/*If more than one match is found.*/
		return `${foundStudents.toString()} are enrollees.`;
	}
	else{
		/*If no match was found.*/
		return `${name} is not an enrollee.`;
	}
}

function addSection(section){
	let sectionedStudents = students.map(function(student){
		return `${student} - section ${section}`;
	});
	return sectionedStudents;
}

function removeStudent(name){
	let capsName = name.toLowerCase();
	capsName = capsName.slice(0, 1).toUpperCase().concat(capsName.slice(1, capsName.length));
	if(students.indexOf(capsName) === -1){
		return "There is nothing to delete here.";
	}
	students.splice(students.indexOf(capsName), 1);
	return `${name} is removed from the students list.`

}